import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { UserlistComponent } from './userlist/userlist.component';
import { DashboardComponent } from './dashboard/dashboard.component';
const adminroutes: Routes = [
	{
	    path: 'admin',
	    component:AdminComponent,
	 	children: [
	 		{ path: 'userlist',component: UserlistComponent },
	 		{ path: 'dashboard',component: DashboardComponent },
	 		{ path: '',component: DashboardComponent}
	 	]
	}
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(adminroutes)
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
