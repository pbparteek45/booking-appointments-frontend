import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { LayoutModule } from './shared/layout.module';
import { UserlistComponent } from './userlist/userlist.component';
import { UserlistService } from './userlist/userlist.service';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  declarations: [AdminComponent,UserlistComponent, DashboardComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    LayoutModule
  ],
  providers: [UserlistService]
})
export class AdminModule { }
