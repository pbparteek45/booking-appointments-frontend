import { Component, OnInit } from '@angular/core';
import { LayoutModule } from '../layout/layout.module';
import { UserlistService } from './userlist.service';
@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

userlist;

  constructor(private UserlistService: UserlistService) { }

  ngOnInit() {

  	this.UserlistService.userList().subscribe(response=>{
  		console.log(response);
      this.userlist = response;
  	});
  	
  }
}
