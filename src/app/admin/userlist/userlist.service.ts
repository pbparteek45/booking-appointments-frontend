import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserlistService {

	url = "http://localhost:8000/api/admin/userlist";

  constructor(private http:HttpClient) { }

  userList(){
  	return this.http.get<any>(this.url);
  }

}
