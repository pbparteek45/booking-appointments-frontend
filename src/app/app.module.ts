import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AdminModule } from './admin/admin.module';
import { UserService } from './user/user.service';
import { AppRoutingModule } from './app-routing.module';
import { UserComponent } from './user/user.component';
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';
import { EnduserComponent } from './enduser/enduser.component';



@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    EnduserComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    AdminModule,
    InternationalPhoneNumberModule
    ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
