import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnduserRoutingModule } from './enduser-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EnduserRoutingModule
  ]
})
export class EnduserModule { }
