import { Component, OnInit } from '@angular/core';

import { FormGroup,FormControl,Validators} from '@angular/forms';

import { UserService } from './user.service';

import { ActivatedRoute,Router,Route } from '@angular/router';

import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  errors;

  constructor(private _userService: UserService, private route: ActivatedRoute) { }


  ngOnInit() {
  }

  form = new FormGroup({
  	first_name: new FormControl('enter the first name',Validators.required),
  	last_name: new FormControl('enter the last name',Validators.required),
  	password: new FormControl,
  	confirm_password: new FormControl,
  	email: new FormControl,
  	mobile_no: new FormControl,
    role: new FormControl
  });

  get firstname(){
  	return this.form.get('first_name');
  }

  onSubmit(){
  	// console.log(this.form.value);
  	this._userService.register(this.form.value).subscribe(response=>{
  		console.log(response)
      if(response.errors){
        this.errors = response.errors;
        console.log(this.errors);
      }

      if(response.redirect){
        console.log('ok');
      }
  	});
  }
}
