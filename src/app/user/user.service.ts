import { Injectable } from '@angular/core';

import{ HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

	url = "http://localhost:8000/api/signup";

  constructor(private http:HttpClient) { }

  register(postData){

  	return this.http.post<any>(this.url,postData);
  }

}
